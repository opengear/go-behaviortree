module bitbucket.org/opengear/go-behaviortree

go 1.13

require (
	bitbucket.org/opengear/go-bigbuff v1.15.1
	github.com/go-test/deep v1.0.8
	github.com/xlab/treeprint v1.1.0
)
